﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Word;
using Word = Microsoft.Office.Interop.Word;

namespace ConsoleApplication1
{
    class Program
    {
        static void Convert(string Filename)
        {
            Word.Application wordApp = null;
            try
            {
                wordApp = new Word.Application();
                // If PasswordDocument is set Word will try to open the file using that password
                // Throwing an exception if that password doesn't work
                Document doc = wordApp.Documents.Open(Filename,
                    PasswordDocument: "rusl", ReadOnly: true);
                doc.SaveAs2(Filename+".pdf", FileFormat: WdSaveFormat.wdFormatPDF);
                wordApp.Quit();
            }
            catch
            {
                if (wordApp != null)
                {
                    wordApp.Quit();
                }
            }
        }

        static void Main(string[] args)
        {
            // in my test word.docx was a "normal" word document, word2.docx was password protected
            Convert(@"C:\drasl\word.docx");
            Convert(@"C:\drasl\word2.docx");
        }
    }
}
